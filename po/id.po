# Indonesian translation for Decibels.
# Copyright (C) 2025 Decibels's COPYRIGHT HOLDER
# This file is distributed under the same license as the Decibels package.
# Andika Triwidada <atriwidada@gnome.org>, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Decibels main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/Incubator/decibels/-/"
"issues\n"
"POT-Creation-Date: 2025-02-10 19:31+0000\n"
"PO-Revision-Date: 2025-02-24 12:11+0800\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesian <Indonesian>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.5\n"

#: data/org.gnome.Decibels.desktop.in.in:3
#: data/org.gnome.Decibels.desktop.in.in:4
#: data/org.gnome.Decibels.metainfo.xml.in.in:4 data/window.blp:8
msgid "Audio Player"
msgstr "Pemutar Audio"

#: data/org.gnome.Decibels.desktop.in.in:5
#: data/org.gnome.Decibels.metainfo.xml.in.in:5
msgid "Play audio files"
msgstr "Memainkan berkas audio"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Decibels.desktop.in.in:13
msgid "music;player;media;audio;decibels;"
msgstr "musik;pemutar;media;audio;decibels;"

#: data/org.gnome.Decibels.metainfo.xml.in.in:12
msgid ""
"An audio player that just plays audio files. It doesn't require an organized "
"music library and won't overload you with tons of functionality."
msgstr ""
"Pemutar audio yang memutar berkas audio begitu saja. Tidak memerlukan "
"pustaka musik yang terorganisir dan tidak akan membebani Anda dengan banyak "
"fungsi."

#: data/org.gnome.Decibels.metainfo.xml.in.in:13
msgid "Audio Player still offers advanced features such as:"
msgstr "Pemutar Audio masih menawarkan fitur-fitur canggih seperti:"

#: data/org.gnome.Decibels.metainfo.xml.in.in:15
msgid "An elegant waveform of the track"
msgstr "Bentuk gelombang trek yang elegan"

#: data/org.gnome.Decibels.metainfo.xml.in.in:16
msgid "Adjustable playback speed"
msgstr "Kecepatan pemutaran yang dapat disesuaikan"

#: data/org.gnome.Decibels.metainfo.xml.in.in:17
msgid "Easy seek controls"
msgstr "Kontrol pencarian yang mudah"

#: data/org.gnome.Decibels.metainfo.xml.in.in:18
msgid "Playing multiple files at the same time"
msgstr "Memutar beberapa berkas secara bersamaan"

#: data/drag-overlay.blp:17
msgid "Drop Here to Open"
msgstr "Jatuhkan Di Sini Untuk Membuka"

#: data/empty.blp:10
msgid "Play Audio Files"
msgstr "Memainkan Berkas Audio"

#: data/empty.blp:11
msgid "Drag and drop audio files here"
msgstr "Seret dan jatuhkan berkas audio di sini"

#: data/empty.blp:18
msgid "_Open…"
msgstr "_Buka…"

#: data/error.blp:11 src/drag-overlay.ts:69 src/window.ts:124 src/window.ts:175
#: src/window.ts:194 src/window.ts:214
msgid "File Cannot Be Played"
msgstr "Berkas Tidak Bisa Diputar"

#: data/error.blp:18
msgid "_Try Again…"
msgstr "_Coba Lagi…"

#: data/header.blp:11
msgid "Main Menu"
msgstr "Menu Utama"

#: data/header.blp:19
msgid "New Window"
msgstr "Jendela Baru"

#: data/header.blp:24
msgid "Open…"
msgstr "Buka…"

#: data/header.blp:31
msgid "Show in _Files"
msgstr "Tampilkan dalam _Berkas"

#: data/header.blp:39
msgid "_Keyboard Shortcuts"
msgstr "Pintasan Papan Ti_k"

#: data/header.blp:44
msgid "_About Audio Player"
msgstr "Tent_ang Pemutar Audio"

#: data/player.blp:66
msgid "Adjust Playback Speed"
msgstr "Menyesuaikan Kecepatan Pemutaran"

#: data/player.blp:78
msgid "Skip Back 10s"
msgstr "Mundur 10 dtk"

#: data/player.blp:105
msgid "Skip Forward 10s"
msgstr "Maju 10 dtk"

#: data/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Umum"

#: data/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tampilkan Pintasan"

#: data/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "New Window"
msgstr "Jendela Baru"

#: data/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Quit"
msgstr "Keluar"

#: data/gtk/help-overlay.blp:30
msgctxt "shortcut window"
msgid "Player"
msgstr "Pemutar"

#: data/gtk/help-overlay.blp:33
msgctxt "shortcut window"
msgid "Open File"
msgstr "Buka Berkas"

#: data/gtk/help-overlay.blp:38
msgctxt "shortcut window"
msgid "Pause/Play"
msgstr "Jeda/Putar"

#: data/gtk/help-overlay.blp:43
msgctxt "shortcut window"
msgid "Go Back 10 Seconds"
msgstr "Mundur 10 Detik"

#: data/gtk/help-overlay.blp:48
msgctxt "shortcut window"
msgid "Go Forward 10 Seconds"
msgstr "Maju 10 Detik"

#: data/gtk/help-overlay.blp:54
msgctxt "shortcut window"
msgid "Playback Rate"
msgstr "Laju Putar"

#: data/gtk/help-overlay.blp:57
msgctxt "shortcut window"
msgid "Increase Playback Rate"
msgstr "Naikkan Laju Putar"

#: data/gtk/help-overlay.blp:62
msgctxt "shortcut window"
msgid "Decrease Playback Rate"
msgstr "Turunkan Laju Putar"

#: data/gtk/help-overlay.blp:67
msgctxt "shortcut window"
msgid "Reset Playback Rate"
msgstr "Atur Ulang Laju Putar"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/application.ts:73
msgid "translator-credits"
msgstr "Andika Triwidada <andika@gmail.com>, 2025."

#: src/drag-overlay.ts:70
msgid "Unable to access dropped files"
msgstr "Tidak bisa mengakses berkas yang dijatuhkan"

#: src/error.ts:42
msgid "An unknown error happened"
msgstr "Terjadi galat yang tidak diketahui"

#: src/stream.ts:271
msgid ""
"File uses a format that cannot be played. Additional media codecs may be "
"required."
msgstr ""
"Berkas menggunakan format yang tidak dapat diputar. Kodek media tambahan "
"mungkin diperlukan."

#: src/stream.ts:283
msgid ""
"An error happened while trying to get information about the file. Please try "
"again."
msgstr ""
"Terjadi galat saat mencoba mendapatkan informasi tentang berkas. Silakan "
"coba lagi."

#: src/stream.ts:294
msgid "File uses an invalid URI"
msgstr "Berkas menggunakan URI yang tidak valid"

#: src/stream.ts:305
msgid "Reading the file’s information timed out. Please try again."
msgstr "Membaca informasi berkas habis waktunya. Silakan coba lagi."

#: src/stream.ts:421
msgid "Unknown File"
msgstr "Berkas Tak Dikenal"

#: src/stream.ts:665
msgid "The selected file doesn’t contain any audio"
msgstr "Berkas yang dipilih tidak berisi audio apa pun"

#: src/volume-button.ts:57
msgid "Muted"
msgstr "Dibisukan"

#: src/volume-button.ts:58
msgid "Full Volume"
msgstr "Volume Penuh"

#. Translators: this is the percentage of the current volume,
#. * as used in the tooltip, eg. "49 %".
#. * Translate the "%d" to "%Id" if you want to use localised digits,
#. * or otherwise translate the "%d" to "%d".
#.
#: src/volume-button.ts:66
#, c-format
msgctxt "volume percentage"
msgid "%d %%"
msgstr "%d %%"

#: src/player.ts:104
msgid "Pause"
msgstr "Jeda"

#: src/player.ts:104
msgid "Play"
msgstr "Putar"

#: src/window.ts:130
msgid "Audio files"
msgstr "Berkas suara"

#: src/window.ts:137
msgid "Open File"
msgstr "Buka Berkas"

#: src/window.ts:176
msgid "No available audio file found"
msgstr "Tidak ditemukan berkas audio yang tersedia"

#: src/window.ts:188
msgid "Directories Cannot Be Played"
msgstr "Direktori Tidak Bisa Diputar"

#: src/window.ts:189
msgid "Please select a file."
msgstr "Silakan pilih suatu berkas."

#: src/window.ts:195
msgid "The selected file is not a regular file."
msgstr "Berkas yang dipilih bukan merupakan berkas biasa."

#: src/window.ts:215
msgid "No file was selected"
msgstr "Tak ada berkas yang dipilih"
